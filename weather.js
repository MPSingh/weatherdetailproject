const appKey = "38e6b94f4a87cd44fba71785979c1965";
var searchInput, searchButton, city, temperature, humidity;

function load() {
    searchInput = document.getElementById("search-txt");
    searchButton = document.getElementById("search-btn");

    city = document.getElementById("city-name");
    temperature = document.getElementById("temperature");
    humidity = document.getElementById("humidity");


    searchInput.addEventListener("keyup", enterPressed);
    searchButton.addEventListener("click", findWeatherDetails);
}


function enterPressed() {
    if (event.key == "Enter") {
        findWeatherDetails();
    }
}

function findWeatherDetails() {
    if (searchInput.value == "") {

    } else {
        var searchLink = "https://api.openweathermap.org/data/2.5/weather?q=" + searchInput.value + "&appid=835427c3cb00a8c74d336a3a7953a39a";
        var request = new XMLHttpRequest();
        request.open("GET", searchLink);
        request.send();

	    request.onreadystatechange = function(){ 
	        if (request.readyState == 4 && request.status == 200){
	           city.innerHTML = searchInput.value.toUpperCase();
	           searchInput.value = "";
	       		result(this);
	    	}

	    	if (request.readyState == 4 && request.status == 404){
	           temperature.innerHTML = "Valid city ?";
	    	}
	    }
	}
}

function result(s){
	var record = JSON.parse(s.response);
	var therm = record.main.temp;
	if(record['cod'] != 404){
		temperature.innerHTML = "Temperature" + "<BR>" + Math.round(therm - 273) + "&#8451;" ; 
		humidity.innerHTML = "Humidity" + "<BR>" + record.main.humidity + "%";
	}
	console.log(record);
}
	 